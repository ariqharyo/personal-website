import datetime

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from first.forms import kegiatan_form
from first.models import kegiatan_model


# Create your views here.
def utama(request):
    response = {}
    return render(request, 'utama.html', response)

def relasi(request):
    response = {}
    return render(request, 'relasi.html', response)

def bukutamu(request):
    response = {}
    return render(request, 'bukutamu.html', response)

def kegiatan(request):
    if(request.method == 'POST'):
        kegiatanForm = kegiatan_form(request.POST)

        if(kegiatanForm.is_valid()):
            kegiatan_Model = kegiatan_model(
                nama_kegiatan = kegiatanForm.cleaned_data['form_nama_kegiatan'],
                kategori = kegiatanForm.cleaned_data['form_kategori'],
                hari_tanggal = kegiatanForm.cleaned_data['form_hari_tanggal']
            )
            kegiatan_Model.save()
    else:
        kegiatanForm = kegiatan_form()

    list_kegiatan = kegiatan_model.objects.all()
    response = {'form' : kegiatanForm, 'list_kegiatan' : list_kegiatan}
    return render(request,'kegiatan.html', response)

def hapusKegiatan(request):
    kegiatan_model.objects.all().delete()
    list_kegiatan = kegiatan_model.objects.all()
    response = {"form" : kegiatan_form, "list_kegiatan" : list_kegiatan}
    return HttpResponseRedirect('/kegiatan/')
