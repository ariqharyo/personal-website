import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from .models import kegiatan_model

class kegiatan_form(forms.Form):
    PILIHAN_KATEGORI = (
        ("Akademik", "Akademik"),
        ("Non-Akademik", "Non-Akademik"),
        ("Pribadi", "Pribadi"))

    form_nama_kegiatan = forms.CharField(max_length=50, label="Nama Kegiatan")
    form_kategori = forms.ChoiceField(choices=PILIHAN_KATEGORI, label="Kategori")
    form_hari_tanggal = forms.DateTimeField(help_text="Format = Bulan/Hari/Tahun")

    class Meta:
        model = kegiatan_model

    def clean_hari_tanggal(self):
        data = self.cleaned_data['form_hari_tanggal']

        if data < datetime.datetime.now:
            raise ValidationError(_('Waktu sudah lewat'))
        return data