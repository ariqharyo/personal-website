import datetime

from django.db import models


# Create your models here.

class kegiatan_model(models.Model):
    PILIHAN_KATEGORI = (
        ("Akademik", "Akademik"),
        ("Non-Akademik", "Non-Akademik"),
        ("Pribadi", "Pribadi")
    )
    nama_kegiatan = models.CharField(max_length = 50)
    kategori = models.CharField(max_length =10, choices=PILIHAN_KATEGORI)
    hari_tanggal = models.DateTimeField()

    class Meta:
        ordering = ['hari_tanggal']

    def __str__(self):
        return self.nama_kegiatan
    
    


